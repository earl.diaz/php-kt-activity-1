<?php require_once "./activity.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S1 Activity</title>
</head>
<body>
    <h3>1.</h3>
    <?php
        echo higherNumber(11, 10)
    ?>

    <h3>2.</h3>
    <?php
        echo helloWorldIn('French')
    ?>

    <h3>3.</h3>
    <?php
        echo gradeChecker(85)
    ?>

    <h3>4.</h3>
    <?php
        echo calculation(10, 10, '+')
    ?>

</body>
</html>