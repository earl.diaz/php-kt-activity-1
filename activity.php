<?php
    function higherNumber($firstNumber, $secondNumber) {
        $output;

        $firstNumber > $secondNumber ? $output = $firstNumber : ($firstNumber === $secondNumber ? $output = "Numbers are the same" : $output = $secondNumber);
        
        return $output;
    }

    function helloWorldIn($language){
        $output;

        $language == 'French' ? $output = 'Bonjour le monde' : ($language == 'Spanish' ? $output = 'Hola Mundo' : ($language == 'Japanese' ? $output = "Kon'nichiwa sekai" : $output = 'Please input a valid language. (French, Spanish, or Japanese only)')); 

        return $output;
    }

    function gradeChecker($grade){
        $output;

        if($grade > 99 || $grade < 0) {
            $output = 'The number must be between 0-99';
        } else if($grade > 89){
            $output = 'Your grade is A';
        } else if($grade > 79) {
            $output = 'Your grade is B';
        } else if($grade > 69) {
            $output = 'Your grade is C';
        } else if($grade > 59) {
            $output = 'Your grade is D';
        } 

        return $output;
    }

    function calculation($firstNumber, $secondNumber, $operator){
        $output;

        switch($operator){
            case '+':
                $output = $firstNumber + $secondNumber;
                break;
            case '-':
                $output = $firstNumber - $secondNumber;
                break;
            case '*':
                $output = $firstNumber * $secondNumber;
                break;
            case '/':
                $output = $firstNumber / $secondNumber;
                break;
            default:
                $output = 'Enter a valid math operator (+, -, *, /)';
        }

        return $output;
    }
?>